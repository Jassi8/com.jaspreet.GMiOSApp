//
//  GitCommitTableVC.swift
//  GMiOSApp
//
//  Created by test on 1/8/21.
//  Copyright © 2021 GM. All rights reserved.
//

import UIKit

struct JSONData: Decodable {
    let name: String
    let protected: Bool
    let commit: Commit
    struct Commit: Decodable {
        let sha: String
        let url: String
    }
}
