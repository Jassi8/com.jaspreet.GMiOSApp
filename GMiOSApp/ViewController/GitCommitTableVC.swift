//
//  GitCommitTableVC.swift
//  GMiOSApp
//
//  Created by test on 1/8/21.
//  Copyright © 2021 GM. All rights reserved.
//

import UIKit

class GitCommitTableVC: UITableViewController {

    var commitList = [JSONData]()
    
    // MARK: - Class Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        parse()
    }
    
    // MARK: - Functions
    func parse() {
        let url = URL(string: ConstantsStruct.urlString)!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            // error handeling
            guard error == nil else {
              print ("Error: \(error!)")
              return
            }
            // error handeling no data
            guard let responseData = data else {
              print("Error: No data")
              return
            }
            // Parse JSON into array of commitListing struct using JSONDecoder
            guard let commitListing = try? JSONDecoder().decode([JSONData].self, from: responseData) else {
                print("Error: Couldn't decode data into commitListing")
                return
            }
            self.commitList = commitListing
            // tableView reloading on main thread
            DispatchQueue.main.async {
              self.tableView.reloadData()
            }
        }
        // execute the HTTP request
        task.resume()
    }
    
    func refreshTableView()
    {
        // Make sure to update UI in main thread
        DispatchQueue.main.async {
          self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commitList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let commitName = commitList[indexPath.row]
        cell.textLabel?.text = "Name: \(commitName.name)"
        let commitSha = commitList[indexPath.row]
        cell.detailTextLabel?.text = "Sha: \(commitSha.commit.sha)"
        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
